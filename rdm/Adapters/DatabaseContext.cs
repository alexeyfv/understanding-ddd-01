using Microsoft.EntityFrameworkCore;
using Cli.DomainLayer;

namespace Cli.Adapters;

public class DatabaseAdapter : DbContext
{
    public DbSet<Ticket> Tickets { get; set; } = default!;
    public DbSet<Worker> Workers { get; set; } = default!;
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Data Source=db.sqlite");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Worker>()
            .HasMany(w => w.AssignedTickets)
            .WithOne(t => t.AssignedWorker)
            .OnDelete(DeleteBehavior.SetNull);
    }
}