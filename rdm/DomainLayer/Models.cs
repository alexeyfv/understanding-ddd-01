namespace Cli.DomainLayer;

public record Ticket
{
    public int Id { get; protected set; }
    public string Content { get; protected set; } = string.Empty;
    public DateTimeOffset Created { get; protected set; }
    public DateTimeOffset Updated { get; protected set; }
    public int? AssignedWorkerId { get; protected set; }
    public Worker? AssignedWorker { get; protected set; }

    public EventBase Handle(UpdateTicketDescription command)
    {
        Updated = DateTimeOffset.Now;
        Content = command.Description;
        return new TicketChanged(this);
    }

    public EventBase Handle(OpenTicketForWorker command)
    {
        AssignedWorkerId = Id;
        Created = Updated = DateTimeOffset.Now;
        Content = command.Content;
        return new TicketChanged(this);
    }

    public EventBase Handle(CloseTicketForWorker command)
    {
        Updated = DateTimeOffset.Now;
        AssignedWorkerId = null;
        AssignedWorker = null;
        return new TicketChanged(this);
    }
}

public record Worker
{
    public int Id { get; protected set; }
    public string Name { get; protected set; } = string.Empty;
    public string Email { get; protected set; } = string.Empty;
    public string Position { get; protected set; } = string.Empty;
    public bool Fired { get; protected set; }
    public DateTimeOffset Created { get; protected set; }
    public DateTimeOffset Updated { get; protected set; }

    // Relations
    public ICollection<Ticket> AssignedTickets { get; protected set; } = new List<Ticket>();

    public EventBase Handle(CreateWorker request)
    {
        Name = request.Name;
        Email = request.Email;
        Position = request.Position;
        Created = Updated = DateTimeOffset.Now;
        return new WorkerCreated(this);
    }
    public EventBase Handle(GetWorkerInfo request)
    {
        return new WorkerInfoReturned(this);
    }

    public EventBase Handle(UpdateWorker request)
    {
        if (request.Name != null) Name = request.Name;
        if (request.Email != null) Email = request.Email;
        if (request.Position != null) Position = request.Position;
        if (request.Name != null || request.Email != null || request.Position != null) Updated = DateTimeOffset.Now;
        return new WorkerUpdated(this);
    }

    public EventBase Handle(FireWorker request)
    {
        Fired = true;
        Updated = DateTimeOffset.Now;
        return new WorkerFired(this);
    }

    public EventBase Handle(OpenTicketForWorker command)
    {
        var ticket = new Ticket();
        ticket.Handle(command);
        AssignedTickets.Add(ticket);
        Updated = DateTimeOffset.Now;
        return new TicketOpenedForWorker(this);
    }

    public EventBase Handle(CloseTicketForWorker command)
    {
        var ticket = AssignedTickets.First(x => x.Id == command.TicketId);
        ticket.Handle(command);
        return new TicketClosedForWorker(this);
    }
}
