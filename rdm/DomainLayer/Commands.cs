namespace Cli.DomainLayer;

// Commands
public abstract record CommandBase;
public abstract record WorkerCommand(int Id) : CommandBase;
public abstract record TicketCommand(int Id) : CommandBase;
public record CreateWorker(string Name, string Email, string Position) : WorkerCommand(0);
public record UpdateWorker(int Id, string? Name, string? Email, string? Position) : WorkerCommand(Id);
public record FireWorker(int Id) : WorkerCommand(Id);
public record GetWorkerInfo(int Id) : WorkerCommand(Id);
public record OpenTicketForWorker(int Id, string Content) : WorkerCommand(Id);
public record CloseTicketForWorker(int Id, int TicketId) : WorkerCommand(Id);
public record GetTicketInfo(int Id) : TicketCommand(Id);
public record UpdateTicketDescription(int Id, string Description) : TicketCommand(Id);
