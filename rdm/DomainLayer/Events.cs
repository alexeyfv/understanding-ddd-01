namespace Cli.DomainLayer;

// Events
public abstract record EventBase;
public record WorkerCreated(Worker Worker) : EventBase;
public record WorkerUpdated(Worker Worker) : EventBase;
public record WorkerFired(Worker Worker) : EventBase;
public record WorkerInfoReturned(Worker Worker) : EventBase;
public record TicketOpenedForWorker(Worker Worker) : EventBase;
public record TicketClosedForWorker(Worker Worker) : EventBase;
public record TicketChanged(Ticket Ticket) : EventBase;
