using Microsoft.EntityFrameworkCore;
using Cli.Adapters;
using Cli.DomainLayer;

namespace Cli.ApplicationLayer;

public class CommandHandler
{
    public EventBase Handle(CommandBase command)
    {
        var @event = command switch
        {
            TicketCommand cmd => Handle(cmd),
            WorkerCommand cmd => Handle(cmd),
            _ => throw new NotSupportedException(),
        };
        return @event;
    }

    private EventBase Handle(TicketCommand command)
    {
        var db = new DatabaseAdapter();
        var ticket = db.Tickets.First(x => x.Id == command.Id);
        var @event = command switch
        {
            UpdateTicketDescription cmd => ticket.Handle(cmd),
            _ => throw new NotSupportedException(),
        };
        db.SaveChanges();
        return @event;
    }

    private EventBase Handle(WorkerCommand command)
    {
        var db = new DatabaseAdapter();

        var @event = command switch
        {
            CreateWorker cmd => db.Workers
                .Add(new Worker()).Entity
                .Handle(cmd),
            UpdateWorker cmd => db.Workers
                .First(x => x.Id == cmd.Id)
                .Handle(cmd),
            FireWorker cmd => db.Workers
                .First(x => x.Id == cmd.Id)
                .Handle(cmd),
            GetWorkerInfo cmd => db.Workers
                .First(x => x.Id == cmd.Id)
                .Handle(cmd),
            OpenTicketForWorker cmd => db.Workers
                .First(x => x.Id == cmd.Id)
                .Handle(cmd),
            CloseTicketForWorker cmd => db.Workers
                .Include(x => x.AssignedTickets)
                .First(x => x.Id == cmd.Id)
                .Handle(cmd),
            _ => throw new NotSupportedException(),
        };

        db.SaveChanges();
        return @event;
    }
}