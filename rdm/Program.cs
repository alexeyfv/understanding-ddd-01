﻿using System.CommandLine;
using Cli.ApplicationLayer;
using Cli.DomainLayer;

var commandHandler = new CommandHandler();

var idOption = new Option<int>(new[] { "-id" }, "Worker id")
{
    IsRequired = true,
    Arity = ArgumentArity.ExactlyOne
};
var ticketIdOption = new Option<int>(new[] { "--ticket-id" }, "Ticket id")
{
    IsRequired = true,
    Arity = ArgumentArity.ExactlyOne
};
var nameOption = new Option<string>(new[] { "-n", "--name" }, "Worker name")
{
    IsRequired = true,
    Arity = ArgumentArity.ExactlyOne
};
var emailOption = new Option<string>(new[] { "-e", "--email" }, "Worker email")
{
    IsRequired = true,
    Arity = ArgumentArity.ExactlyOne
};
var positionOption = new Option<string>(new[] { "-p", "--position" }, "Worker position")
{
    IsRequired = true,
    Arity = ArgumentArity.ExactlyOne
};
var updateNameOption = new Option<string>(new[] { "-n", "--name" }, "Worker name")
{
    Arity = ArgumentArity.ExactlyOne
};
var updateEmailOption = new Option<string>(new[] { "-e", "--email" }, "Worker email")
{
    Arity = ArgumentArity.ExactlyOne
};
var updatePositionOption = new Option<string>(new[] { "-p", "--position" }, "Worker position")
{
    Arity = ArgumentArity.ExactlyOne
};
var descriptionOption = new Option<string>(new[] { "-c", "--content" }, "Content of the ticket")
{
    IsRequired = true,
    Arity = ArgumentArity.ExactlyOne
};

var createWorkerCommand = new Command("add", "Add worker");
createWorkerCommand.Add(nameOption);
createWorkerCommand.Add(emailOption);
createWorkerCommand.Add(positionOption);
createWorkerCommand.SetHandler((name, email, position) =>
{
    EventBase e = commandHandler.Handle(new CreateWorker(name, email, position));
    Console.WriteLine(e);
}, nameOption, emailOption, positionOption);

var updateWorkerCommand = new Command("update", "Update worker");
updateWorkerCommand.Add(idOption);
updateWorkerCommand.Add(updateNameOption);
updateWorkerCommand.Add(updateEmailOption);
updateWorkerCommand.Add(updatePositionOption);
updateWorkerCommand.SetHandler((id, name, email, position) =>
{
    EventBase e = commandHandler.Handle(new UpdateWorker(id, name, email, position));
    Console.WriteLine(e);
}, idOption, updateNameOption, updateEmailOption, updatePositionOption);

var fireWorkeCommand = new Command("fire", "Fire worker");
fireWorkeCommand.Add(idOption);
fireWorkeCommand.SetHandler((id) =>
{
    EventBase e = commandHandler.Handle(new FireWorker(id));
    Console.WriteLine(e);
}, idOption);

var infoWorkerCommand = new Command("info", "Get worker's info");
infoWorkerCommand.Add(idOption);
infoWorkerCommand.SetHandler((id) =>
{
    EventBase e = commandHandler.Handle(new GetWorkerInfo(id));
    Console.WriteLine(e);
}, idOption);

var workerCommand = new Command("worker", "Worker related commands");
workerCommand.Add(createWorkerCommand);
workerCommand.Add(updateWorkerCommand);
workerCommand.Add(fireWorkeCommand);
workerCommand.Add(infoWorkerCommand);

var openCommand = new Command("open", "Open ticket for worker");
openCommand.Add(idOption);
openCommand.Add(descriptionOption);
openCommand.SetHandler((id, content) =>
{
    EventBase e = commandHandler.Handle(new OpenTicketForWorker(id, content));
    Console.WriteLine(e);
}, idOption, descriptionOption);

var closeCommand = new Command("close", "Close ticket for worker");
closeCommand.SetHandler((id, ticketId) =>
{
    EventBase e = commandHandler.Handle(new CloseTicketForWorker(id, ticketId));
    Console.WriteLine(e);
}, idOption, ticketIdOption);

var updateCommand = new Command("update", "Update ticket info");
updateCommand.Add(idOption);
updateCommand.Add(descriptionOption);
updateCommand.SetHandler((id, desc) =>
{
    EventBase e = commandHandler.Handle(new UpdateTicketDescription(id, desc));
}, idOption, descriptionOption);

var ticketCommand = new Command("ticket", "Ticket related commands");
ticketCommand.Add(openCommand);
ticketCommand.Add(closeCommand);
ticketCommand.Add(updateCommand);

var root = new RootCommand();
root.Add(workerCommand);
root.Add(ticketCommand);

return root.Invoke(args);
