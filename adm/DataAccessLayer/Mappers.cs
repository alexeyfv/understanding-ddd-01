using Cli.DomainLayer;

namespace Cli.DataAccessLayer;

public class TicketMapper
{
    public Ticket Map(TicketDbo from) =>
        new()
        {
            Id = from.Id,
            Description = from.Description,
            Created = from.Created,
            Updated = from.Updated
        };

    public TicketDbo Map(Ticket from, TicketDbo to)
    {
        to.Id = from.Id;
        to.Description = from.Description;
        to.Created = from.Created;
        to.Updated = from.Updated;
        return to;
    }

    public TicketDbo Map(Ticket from) =>
        new()
        {
            Id = from.Id,
            Description = from.Description,
            Created = from.Created,
            Updated = from.Updated,
        };
}

public class WorkerMapper
{

    public Worker Map(WorkerDbo from) =>
        new()
        {
            Id = from.Id,
            Name = from.Name,
            Email = from.Email,
            Position = from.Position,
            Fired = from.Fired,
            Created = from.Created,
            Updated = from.Updated
        };

    public WorkerDbo Map(Worker from, WorkerDbo to) =>
        to with
        {
            Id = from.Id,
            Name = from.Name,
            Email = from.Email,
            Position = from.Position,
            Fired = from.Fired,
            Created = from.Created,
            Updated = from.Updated
        };

    public WorkerDbo Map(Worker from) =>
        new()
        {
            Id = from.Id,
            Name = from.Name,
            Email = from.Email,
            Position = from.Position,
            Fired = from.Fired,
            Created = from.Created,
            Updated = from.Updated
        };
}