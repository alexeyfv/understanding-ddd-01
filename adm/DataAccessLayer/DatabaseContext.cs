
using Microsoft.EntityFrameworkCore;

namespace Cli.DataAccessLayer;

public class DatabaseContext : DbContext
{
    public DbSet<TicketDbo> Tickets { get; set; } = default!;
    public DbSet<WorkerDbo> Workers { get; set; } = default!;
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Data Source=db.sqlite");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<WorkerDbo>()
            .HasMany(w => w.AssignedTickets)
            .WithOne(t => t.AssignedWorker)
            .OnDelete(DeleteBehavior.SetNull);
    }
}