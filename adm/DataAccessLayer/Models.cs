namespace Cli.DataAccessLayer;

public record TicketDbo
{
    public int Id { get; set; }
    public string Description { get; set; } = string.Empty;
    public DateTimeOffset Created { get; set; }
    public DateTimeOffset Updated { get; set; }

    // Relations
    public int? AssignedWorkerId { get; set; }
    public WorkerDbo? AssignedWorker { get; set; }
}


public record WorkerDbo
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Email { get; set; } = string.Empty;
    public string Position { get; set; } = string.Empty;
    public bool Fired { get; set; }
    public DateTimeOffset Created { get; set; }
    public DateTimeOffset Updated { get; set; }

    // Relations
    public ICollection<TicketDbo> AssignedTickets { get; set; } = new List<TicketDbo>();
}