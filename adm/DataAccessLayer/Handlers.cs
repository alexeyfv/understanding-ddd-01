using Cli.DomainLayer;

namespace Cli.DataAccessLayer;

public class CreateWorkerHandler
{
    public void Handle(CreateWorkerRequest request)
    {
        var db = new DatabaseContext();
        var mapper = new WorkerMapper();
        var now = DateTimeOffset.Now;
        var dbo = mapper.Map(request.Worker with
        {
            Created = now,
            Updated = now
        });
        db.Workers.Add(dbo);
        db.SaveChanges();
    }
}

public class GetWorkerHandler
{
    public Worker Handle(GetWorkerInfoQuery request)
    {
        var db = new DatabaseContext();
        var dbo = db.Workers.First(x => x.Id == request.Id);
        var mapper = new WorkerMapper();
        return mapper.Map(dbo);
    }
}


public class UpdateWorkerHandler
{
    public void Handle(UpdateWorkerRequest request)
    {
        var db = new DatabaseContext();
        var mapper = new WorkerMapper();
        var now = DateTimeOffset.Now;
        var dbo = db.Workers.First(x => x.Id == request.Worker.Id);
        mapper.Map(request.Worker with
        {
            Created = now,
            Updated = now
        }, dbo);
        db.SaveChanges();
    }
}

public class DeleteWorkerHandler
{
    public void Handle(FireWorkerRequest request)
    {
        var db = new DatabaseContext();
        var mapper = new WorkerMapper();
        var now = DateTimeOffset.Now;
        var dbo = db.Workers.First(x => x.Id == request.Id);
        dbo.Fired = true;
        dbo.Updated = DateTimeOffset.Now;
        db.SaveChanges();
    }
}

public class OpenTicketHandler
{
    public Ticket Handle(OpenTicketRequest request)
    {
        var db = new DatabaseContext();
        var mapper = new TicketMapper();
        var now = DateTimeOffset.Now;
        var workerDbo = db.Workers.First(x => x.Id == request.WorkerId);
        var ticketDbo = new TicketDbo()
        {
            AssignedWorkerId = workerDbo.Id,
            Created = now,
            Updated = now,
            Description = request.Content
        };
        workerDbo.AssignedTickets.Add(ticketDbo);
        workerDbo.Updated = DateTimeOffset.Now;
        db.SaveChanges();
        return mapper.Map(ticketDbo);
    }
}
public class GetTicketHandler
{
    public Ticket Handle(GetTicketRequest request)
    {
        var db = new DatabaseContext();
        var mapper = new TicketMapper();
        var now = DateTimeOffset.Now;
        var ticketDbo = db.Tickets.First(x => x.Id == request.Id);
        return mapper.Map(ticketDbo);
    }
}

public class UpsertTicketHandler
{
    public Ticket Handle(UpsertTicketRequest request)
    {
        var db = new DatabaseContext();
        var mapper = new TicketMapper();
        var now = DateTimeOffset.Now;
        var ticketDbo = db.Tickets.FirstOrDefault(x => x.Id == request.Ticket.Id);
        if (ticketDbo == null) ticketDbo = mapper.Map(request.Ticket);
        else mapper.Map(request.Ticket, ticketDbo);
        ticketDbo.Updated = DateTimeOffset.Now;
        db.SaveChanges();
        return mapper.Map(ticketDbo);
    }
}

public class CloseTicketHandler
{
    public void Handle(CloseTicketRequest request)
    {
        var db = new DatabaseContext();
        var mapper = new TicketMapper();
        var dbo = db.Workers.First(x => x.Id == request.WorkerId);
        var ticketDbo = dbo.AssignedTickets.First(x => x.Id == request.TicketId);
        dbo.AssignedTickets.Remove(ticketDbo);
        ticketDbo.Updated = dbo.Updated = DateTimeOffset.Now;
        db.SaveChanges();
    }
}
