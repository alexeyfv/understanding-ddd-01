﻿

using System.CommandLine;
using Cli.DomainLayer;
using Cli.DataAccessLayer;
using Spectre.Console;

var idOption = new Option<int>(new[] { "-id" }, "Worker's id")
{
    IsRequired = true,
    Arity = ArgumentArity.ExactlyOne
};

var nameOption = new Option<string>(new[] { "-n", "--name" }, "Worker's name")
{
    IsRequired = true,
    Arity = ArgumentArity.ExactlyOne
};
var emailOption = new Option<string>(new[] { "-e", "--email" }, "Worker's email")
{
    IsRequired = true,
    Arity = ArgumentArity.ExactlyOne
};
var positionOption = new Option<string>(new[] { "-p", "--position" }, "Worker's position")
{
    IsRequired = true,
    Arity = ArgumentArity.ExactlyOne
};

var updateNameOption = new Option<string>(new[] { "-n", "--name" }, "Worker's name")
{
    Arity = ArgumentArity.ExactlyOne
};
var updateEmailOption = new Option<string>(new[] { "-e", "--email" }, "Worker's email")
{
    Arity = ArgumentArity.ExactlyOne
};
var updatePositionOption = new Option<string>(new[] { "-p", "--position" }, "Worker's position")
{
    Arity = ArgumentArity.ExactlyOne
};
var contentOption = new Option<string>(new[] { "-c", "--content" }, "Content of the ticket")
{
    IsRequired = true,
    Arity = ArgumentArity.ExactlyOne
};

var createWorkerCommand = new Command("add", "Add worker");
createWorkerCommand.Add(nameOption);
createWorkerCommand.Add(emailOption);
createWorkerCommand.Add(positionOption);
createWorkerCommand.SetHandler((name, email, position) =>
{
    var handler = new CreateWorkerHandler();
    var worker = new Worker()
    {
        Name = name,
        Email = email,
        Position = position
    };
    handler.Handle(new(worker));
}, nameOption, emailOption, positionOption);

var updateWorkerCommand = new Command("update", "Update worker");
updateWorkerCommand.Add(idOption);
updateWorkerCommand.Add(updateNameOption);
updateWorkerCommand.Add(updateEmailOption);
updateWorkerCommand.Add(updatePositionOption);
updateWorkerCommand.SetHandler((id, name, email, position) =>
{
    var handler = new UpdateWorkerHandler();
    var worker = new Worker()
    {
        Id = id,
        Name = name,
        Email = email,
        Position = position
    };
    handler.Handle(new(worker));
}, idOption, nameOption, emailOption, positionOption);

var fireWorkeCommand = new Command("fire", "Fire worker");
fireWorkeCommand.Add(idOption);
fireWorkeCommand.SetHandler((id) =>
{
    var handler = new DeleteWorkerHandler();
    handler.Handle(new(id));
}, idOption);

var infoWorkerCommand = new Command("info", "Get worker's info");
infoWorkerCommand.Add(idOption);
infoWorkerCommand.SetHandler((id) =>
{
    var handler = new GetWorkerHandler();
    var worker = handler.Handle(new GetWorkerInfoQuery(id));
    var table = new Table();
    table.AddColumns("Id", "Name", "Email", "Position", "Fired", "Created", "Updated");
    table.AddRow(
        $"{worker.Id}",
        worker.Name,
        worker.Email,
        worker.Position,
        $"{worker.Fired}",
        $"{worker.Created:yyyy-MM-dd HH:mm:ss}",
        $"{worker.Updated:yyyy-MM-dd HH:mm:ss}");
    AnsiConsole.Write(table);
}, idOption);

var workerCommand = new Command("worker", "Worker related commands");
workerCommand.Add(createWorkerCommand);
workerCommand.Add(updateWorkerCommand);
workerCommand.Add(fireWorkeCommand);
workerCommand.Add(infoWorkerCommand);

var openCommand = new Command("open", "Open ticket for worker");
openCommand.Add(idOption);
openCommand.Add(contentOption);
openCommand.SetHandler((id, content) =>
{
    var handler = new OpenTicketHandler();
    var t = handler.Handle(new(id, content));
    var table = new Table();
    table.AddColumns("Id", "Content", "Created", "Updated");
    table.AddRow(
        $"{t.Id}",
        t.Description,
        $"{t.Created:yyyy-MM-dd HH:mm:ss}",
        $"{t.Updated:yyyy-MM-dd HH:mm:ss}");
    AnsiConsole.Write(table);
}, idOption, contentOption);
var closeCommand = new Command("close", "Close ticket for worker");

var updateCommand = new Command("update", "Update ticket info");
updateCommand.Add(idOption);
updateCommand.Add(contentOption);
updateCommand.SetHandler((id, content) =>
{
    var handler = new UpdateTicketContentHandler();
    var t = handler.Handle(new(id, content));
    var table = new Table();
    table.AddColumns("Id", "Content", "Created", "Updated");
    table.AddRow(
        $"{t.Id}",
        t.Description,
        $"{t.Created:yyyy-MM-dd HH:mm:ss}",
        $"{t.Updated:yyyy-MM-dd HH:mm:ss}");
    AnsiConsole.Write(table);
}, idOption, contentOption);

var ticketCommand = new Command("ticket", "Ticket related commands");
ticketCommand.Add(openCommand);
ticketCommand.Add(closeCommand);
ticketCommand.Add(updateCommand);

var root = new RootCommand();
root.Add(workerCommand);
root.Add(ticketCommand);

return root.Invoke(args);
