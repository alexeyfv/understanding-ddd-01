namespace Cli.DomainLayer;

public record CreateWorkerRequest(Worker Worker);
public record UpdateWorkerRequest(Worker Worker);
public record FireWorkerRequest(int Id);
public record OpenTicketRequest(int WorkerId, string Content);
public record CloseTicketRequest(int WorkerId, int TicketId);
public record UpdateTicketContntRequest(int Id, string Content);

public record GetTicketRequest(int Id);
public record UpsertTicketRequest(Ticket Ticket);