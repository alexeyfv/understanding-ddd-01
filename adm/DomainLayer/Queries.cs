namespace Cli.DomainLayer;

public record GetWorkerInfoQuery(int Id);
