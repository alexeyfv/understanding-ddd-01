using Cli.DataAccessLayer;

namespace Cli.DomainLayer;

public class UpdateTicketContentHandler
{
    public Ticket Handle(UpdateTicketContntRequest request)
    {
        var getTicketHandler = new GetTicketHandler();
        var upsertTicketHandler = new UpsertTicketHandler();
        var ticket = getTicketHandler.Handle(new(request.Id));
        ticket.Description = request.Content;
        return upsertTicketHandler.Handle(new(ticket));
    }
}