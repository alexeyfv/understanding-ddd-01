namespace Cli.DomainLayer;

public record ChangeLogRecord
{
    public int Id { get; set; }
    public string Description { get; set; } = string.Empty;
}

public record Ticket
{
    public int Id { get; set; }
    public string Description { get; set; } = string.Empty;
    public DateTimeOffset Created { get; set; }
    public DateTimeOffset Updated { get; set; }
}

public record Worker
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Email { get; set; } = string.Empty;
    public string Position { get; set; } = string.Empty;
    public bool Fired { get; set; }
    public DateTimeOffset Created { get; set; }
    public DateTimeOffset Updated { get; set; }
}